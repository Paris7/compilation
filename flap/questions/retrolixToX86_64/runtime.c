#include <stdint.h>
#include <stdio.h>

int64_t add_eight_int(int64_t i1, int64_t i2, int64_t i3, int64_t i4,
                      int64_t i5, int64_t i6, int64_t i7, int64_t i8) {
  return i1 + i2 + i3 + i4 + i5 + i6 + i7 + i8;
}

int64_t observe_int(int64_t n) {
  printf("%ld", n);

  return n;
}
