open Position
open Error
open HopixAST

(** [error pos msg] reports execution error messages. *)
let error positions msg = errorN "execution" positions msg

(** Every expression of Hopix evaluates into a [value].

    The [value] type is not defined here. Instead, it will be defined
    by instantiation of following ['e gvalue] with ['e = environment].
    Why? The value type and the environment type are mutually recursive
    and since we do not want to define them simultaneously, this
    parameterization is a way to describe how the value type will use
    the environment type without an actual definition of this type. *)
type 'e gvalue =
  | VInt of Mint.t
  | VChar of char
  | VString of string
  | VUnit
  | VTagged of constructor * 'e gvalue list
  | VTuple of 'e gvalue list
  | VRecord of (label * 'e gvalue) list
  | VLocation of Memory.location
  | VClosure of 'e * pattern located * expression located
  | VPrimitive of string * ('e gvalue Memory.t -> 'e gvalue -> 'e gvalue)

(** Two values for booleans. *)
let ptrue = VTagged (KId "True", [])

let pfalse = VTagged (KId "False", [])

(** We often need to check that a value has a specific shape.
    To that end, we introduce the following coercions. A
    coercion of type [('a, 'e)] coercion tries to convert an
    Hopix value into a OCaml value of type ['a]. If this conversion
    fails, it returns [None]. *)

type ('a, 'e) coercion = 'e gvalue -> 'a option

let fail = None
let ret x = Some x

let value_as_int = function
  | VInt x -> ret x
  | _ -> fail
;;

let value_as_char = function
  | VChar c -> ret c
  | _ -> fail
;;

let value_as_string = function
  | VString s -> ret s
  | _ -> fail
;;

let value_as_tagged = function
  | VTagged (k, vs) -> ret (k, vs)
  | _ -> fail
;;

let value_as_record = function
  | VRecord fs -> ret fs
  | _ -> fail
;;

let value_as_location = function
  | VLocation l -> ret l
  | _ -> fail
;;

let value_as_closure = function
  | VClosure (e, p, b) -> ret (e, p, b)
  | _ -> fail
;;

let value_as_primitive = function
  | VPrimitive (p, f) -> ret (p, f)
  | _ -> fail
;;

let value_as_bool = function
  | VTagged (KId "True", []) -> true
  | VTagged (KId "False", []) -> false
  | _ -> assert false
;;

(** It is also very common to have to inject an OCaml value into
    the types of Hopix values. That is the purpose of a wrapper. *)
type ('a, 'e) wrapper = 'a -> 'e gvalue

let int_as_value x = VInt x
let bool_as_value b = if b then ptrue else pfalse

(** The flap toplevel needs to print the result of evaluations. This is
    especially useful for debugging and testing purpose. Do not modify
    the code of this function since it is used by the testsuite. *)
let print_value m v =
  (* To avoid to print large (or infinite) values, we stop at depth 5. *)
  let max_depth = 5 in
  let rec print_value d v =
    if d >= max_depth
    then "..."
    else (
      match v with
      | VInt x -> Mint.to_string x
      | VChar c -> "'" ^ Char.escaped c ^ "'"
      | VString s -> "\"" ^ String.escaped s ^ "\""
      | VUnit -> "()"
      | VLocation a -> print_array_value d (Memory.dereference m a)
      | VTagged (KId k, []) -> k
      | VTagged (KId k, vs) -> k ^ print_tuple d vs
      | VTuple vs -> print_tuple d vs
      | VRecord fs ->
        "{"
        ^ String.concat
            ", "
            (List.map (fun (LId f, v) -> f ^ " = " ^ print_value (d + 1) v) fs)
        ^ "}"
      | VClosure _ -> "<fun>"
      | VPrimitive (s, _) -> Printf.sprintf "<primitive: %s>" s)
  and print_tuple d vs =
    "(" ^ String.concat ", " (List.map (print_value (d + 1)) vs) ^ ")"
  and print_array_value d block =
    let r = Memory.read block in
    let n = Mint.to_int (Memory.size block) in
    "[ "
    ^ String.concat
        ", "
        List.(
          map
            (fun i -> print_value (d + 1) (r (Mint.of_int i)))
            (ExtStd.List.range 0 (n - 1)))
    ^ " ]"
  in
  print_value 0 v
;;

let print_values m vs = String.concat "; " (List.map (print_value m) vs)

module Environment : sig
  (** Evaluation environments map identifiers to values. *)
  type t

  (** The empty environment. *)
  val empty : t

  (** [bind env x v] extends [env] with a binding from [x] to [v]. *)
  val bind : t -> identifier -> t gvalue -> t

  (** [update pos x env v] modifies the binding of [x] in [env] so
      that [x ↦ v] ∈ [env]. *)
  val update : Position.t -> identifier -> t -> t gvalue -> unit

  (** [lookup pos x env] returns [v] such that [x ↦ v] ∈ env. *)
  val lookup : Position.t -> identifier -> t -> t gvalue

  (** [UnboundIdentifier (x, pos)] is raised when [update] or
      [lookup] assume that there is a binding for [x] in [env],
      where there is no such binding. *)
  exception UnboundIdentifier of identifier * Position.t

  (** [last env] returns the latest binding in [env] if it exists. *)
  val last : t -> (identifier * t gvalue * t) option

  (** [print env] returns a human readable representation of [env]. *)
  val print : t gvalue Memory.t -> t -> string
end = struct
  type t =
    | EEmpty
    | EBind of identifier * t gvalue ref * t

  let empty = EEmpty
  let bind e x v = EBind (x, ref v, e)

  exception UnboundIdentifier of identifier * Position.t

  let lookup' pos x =
    let rec aux = function
      | EEmpty -> raise (UnboundIdentifier (x, pos))
      | EBind (y, v, e) -> if x = y then v else aux e
    in
    aux
  ;;

  let lookup pos x e = !(lookup' pos x e)
  let update pos x e v = lookup' pos x e := v

  let last = function
    | EBind (x, v, e) -> Some (x, !v, e)
    | EEmpty -> None
  ;;

  let print_binding m (Id x, v) = x ^ " = " ^ print_value m !v

  let print m e =
    let b = Buffer.create 13 in
    let push x v = Buffer.add_string b (print_binding m (x, v)) in
    let rec aux = function
      | EEmpty -> Buffer.contents b
      | EBind (x, v, EEmpty) ->
        push x v;
        aux EEmpty
      | EBind (x, v, e) ->
        push x v;
        Buffer.add_string b "\n";
        aux e
    in
    aux e
  ;;
end

(** We have everything we need now to define [value] as an instantiation
    of ['e gvalue] with ['e = Environment.t], as promised. *)
type value = Environment.t gvalue

(** The following higher-order function lifts a function [f] of type
    ['a -> 'b] as a [name]d Hopix primitive function, that is, an
    OCaml function of type [value -> value]. *)
let primitive name ?(error = fun () -> assert false) coercion wrapper f : value =
  VPrimitive
    ( name
    , fun x ->
        match coercion x with
        | None -> error ()
        | Some x -> wrapper (f x) )
;;

type runtime =
  { memory : value Memory.t
  ; environment : Environment.t
  }

type observable =
  { new_memory : value Memory.t
  ; new_environment : Environment.t
  }

(** [primitives] is an environment that contains the implementation
    of all primitives (+, <, ...). *)
let primitives =
  let intbin name out op =
    let error m v =
      Printf.eprintf "Invalid arguments for `%s': %s\n" name (print_value m v);
      assert false (* By typing. *)
    in
    VPrimitive
      ( name
      , fun m -> function
          | VInt x ->
            VPrimitive
              ( name
              , fun m -> function
                  | VInt y -> out (op x y)
                  | v -> error m v )
          | v -> error m v )
  in
  let bind_all what l x =
    List.fold_left (fun env (x, v) -> Environment.bind env (Id x) (what x v)) x l
  in
  (* Define arithmetic binary operators. *)
  let binarith name = intbin name (fun x -> VInt x) in
  let binarithops = Mint.[ "`+`", add; "`-`", sub; "`*`", mul; "`/`", div ] in
  (* Define arithmetic comparison operators. *)
  let cmparith name = intbin name bool_as_value in
  let cmparithops =
    [ "`=?`", ( = ); "`<?`", ( < ); "`>?`", ( > ); "`>=?`", ( >= ); "`<=?`", ( <= ) ]
  in
  let boolbin name out op =
    VPrimitive
      ( name
      , fun _ x ->
          VPrimitive (name, fun _ y -> out (op (value_as_bool x) (value_as_bool y))) )
  in
  let boolarith name = boolbin name (fun x -> if x then ptrue else pfalse) in
  let boolarithops = [ "`||`", ( || ); "`&&`", ( && ) ] in
  let generic_printer =
    VPrimitive
      ( "print"
      , fun m v ->
          output_string stdout (print_value m v);
          flush stdout;
          VUnit )
  in
  let print s =
    output_string stdout s;
    flush stdout;
    VUnit
  in
  let print_int =
    VPrimitive
      ( "print_int"
      , fun _ -> function
          | VInt x -> print (Mint.to_string x)
          | _ -> assert false (* By typing. *) )
  in
  let print_string =
    VPrimitive
      ( "print_string"
      , fun _ -> function
          | VString x -> print x
          | _ -> assert false (* By typing. *) )
  in
  let bind' x w env = Environment.bind env (Id x) w in
  Environment.empty
  |> bind_all binarith binarithops
  |> bind_all cmparith cmparithops
  |> bind_all boolarith boolarithops
  |> bind' "print" generic_printer
  |> bind' "print_int" print_int
  |> bind' "print_string" print_string
  |> bind' "true" ptrue
  |> bind' "false" pfalse
  |> bind' "nothing" VUnit
;;

let initial_runtime () =
  { memory = Memory.create (640 * 1024 (* should be enough. -- B.Gates *))
  ; environment = primitives
  }
;;

let rec evaluate runtime ast =
  try
    let runtime' = List.fold_left definition runtime ast in
    runtime', extract_observable runtime runtime'
  with
  | Environment.UnboundIdentifier (Id x, pos) ->
    Error.error "interpretation" pos (Printf.sprintf "`%s' is unbound." x)

(** [definition pos runtime d] evaluates the new definition [d]
    into a new runtime [runtime']. In the specification, this
    is the judgment:

    E, M ⊢ dv ⇒ E', M' *)
and definition runtime d =
  match Position.value d with
  | DefineValue vd -> value_definition runtime vd
  (* On ignore tout ce qui n'est pas une value car on interprète *)
  | _ -> runtime

and value_definition runtime = function
  | SimpleValue (id, _, expr) ->
    (* Ici on associe l'ID et l'expression
       => Retourne l'environnement modifié *)
    let env' =
      Environment.bind
        runtime.environment
        id.value
        (expression' runtime.environment runtime.memory expr)
    in
    { runtime with environment = env' }
  | RecFunctions rf ->
    (* Ici on ajoute les noms des fonctions à l'environnement
       pour qu'elles puissent s'appeller dans leur corps de fonction
       => Retourne l'environnement modifié *)
    { runtime with environment = define_rec runtime.environment rf }

and define_rec env rf =
  (* Ajoute les fonctions récursives dans l'environnement
     par défaut on dit qu'elle renvoie Unit *)
  let env' =
    List.fold_left
      (fun curr_env (id, _, _) -> Environment.bind curr_env id.value VUnit)
      env
      rf
  in
  (* On associe les fonctions avec leur contenu en ignorant le type via
     une closure en les rajoutant à l'environnement *)
  List.iter
    ((fun env'' (name, _, FunctionDefinition (pattern, expr)) ->
       Environment.update name.position name.value env'' (VClosure (env'', pattern, expr)))
       env')
    rf;
  env'

and expression' environment memory e =
  expression (position e) environment memory (value e)

(** [expression pos runtime e] evaluates into a value [v] if

    E, M ⊢ e ⇓ v, M'

    and E = [runtime.environment], M = [runtime.memory]. *)
and expression pos environment memory = function
  | Literal l -> literal_expression l.value
  | Variable (id, _) -> variable_value id environment
  | Tagged (constructor, _, list_t) ->
    (* On ignore le type car on interprète *)
    VTagged (constructor.value, List.map (expression' environment memory) list_t)
  | Record (labels, _) ->
    VRecord (List.map (pair_labels_gvalue environment memory) labels)
  | Field (expr, label, _) ->
    (* On ignore la liste de type *)
    field_value expr label environment memory
  | Tuple [] ->
    (* Cas pour le Tuple vide. Un tuple vide ne contient rien (logique),
       donc on utilise un VUnit *)
    VUnit
  | Tuple list_expr -> tuple_value list_expr environment memory
  | Sequence list_expr -> sequence_value list_expr environment memory
  | Define (value_def, expr) -> define_value value_def expr environment memory
  | Fun (FunctionDefinition (pattern, expr)) -> VClosure (environment, pattern, expr)
  | Apply (f, x) -> apply_expression f x environment memory
  | Ref ref -> ref_value ref environment memory
  | Assign (expr1, expr2) ->
    assign_value expr1 expr2 environment memory;
    VUnit
  | Read read -> read_value read environment memory
  | Case (expr, branches) -> case_value expr branches environment memory
  | IfThenElse (expr1, expr2, expr3) ->
    if_then_else_value expr1 expr2 expr3 environment memory
  | While (expr1, expr2) -> while_value expr1 expr2 environment memory pos
  | For (id, expr1, expr2, expr3) -> for_value id expr1 expr2 expr3 environment memory
  | TypeAnnotation _ ->
    (* On ignore le type car on interprète *)
    VUnit

(** Variable: On cherche la variable dans l'environnement *)
and variable_value id environment = Environment.lookup id.position id.value environment

(** Tuple: On applique à chaque expression de la liste l'opération de calcul de sa valeur *)
and tuple_value list_expr environment memory =
  VTuple (List.map (expression' environment memory) list_expr)

(** Sequence: on evalue chaque expression,
    puis on récupère la dernière à avoir été évalué.
    Le dernier élément se trouve facilement en faisant un reverse de liste
    et en récupérant la tête. *)
and sequence_value list_expr environment memory =
  let vs = List.map (expression' environment memory) list_expr in
  List.hd (List.rev vs)

(** Define: On évalue la définition local puis on en fait un nouveau runtime et
    on calcule la valeur de l'expression. *)
and define_value value_def expr environment memory =
  let runtime = value_definition { environment; memory } value_def in
  expression' runtime.environment runtime.memory expr

(** Reference: On alloue de la mémoire pour le résultat du calcul de l'expression *)
and ref_value ref environment memory =
  let dref = expression' environment memory ref in
  VLocation (Memory.allocate memory Mint.one dref)

(** Lecture: On va lire l'espace mémoire *)
and read_value read environment memory =
  let loc = value_as_location (expression' environment memory read) in
  match loc with
  | Some adr ->
    Memory.read (Memory.dereference memory adr) Mint.zero
    (* On lis la valeur de la mémoire *)
  | None -> error [ position read ] "Erreur read"

(** Case: On effectue le pattern matching des branches associés à notre case,
    et on renvoie l'environnement modifié après avoir calculer toute les branches. *)
and case_value expr branches environment memory =
  (* On calcule d'abord l'expression *)
  let expr' = expression' environment memory expr in
  (* On évalue les branches *)
  let branches' =
    List.fold_left
      (fun acc branch ->
        match acc with
        | [] ->
          (* Faut évaluer la branche avec l'expression *)
          let (Branch (pat, branch')) = branch.value in
          (match pattern environment pat.value expr' with
           | Some env -> [ expression' env memory branch' ]
           | None -> [])
        | _ -> acc)
      []
      branches
  in
  (* On fait le match avec ce qu'on a comme info *)
  match branches' with
  | [] -> error [ expr.position ] "erreur"
  | _ as env -> List.hd env

(** For: On va calculer les valeurs de l'expression (ici expr3)
    à chaque itération de la boucle *)
and for_value id expr1 expr2 expr3 environment memory =
  let borne_inf = value_as_int (expression' environment memory expr1) in
  let borne_sup = value_as_int (expression' environment memory expr2) in
  match borne_inf, borne_sup with
  (* On regarde que les borne_inf et borne_sup ont bien une valeur d'entier
     Si c'est le cas, alors nous sommes bien dans une boucle for et on effectue
     ses opérations *)
  | Some borne_inf, Some borne_sup ->
    boucle_for id borne_inf borne_sup expr3 environment memory
  (* On appelle une seconde fonction car pour évalué la boucle for, il faudra
     rappeler la boucle en augmentant l'indice de la borne inférieur de 1
     à chaque appelle. *)
  | _ -> error [ expr1.position; expr2.position ] "erreur"

and boucle_for id borne_inf borne_sup expr3 environment memory =
  if borne_inf <= borne_sup (* Cas où nous sommes dans la boucle *)
  then (
    let env' =
      (* On lis l'identifier avec la borne inférieur *)
      Environment.bind environment id.value (int_as_value borne_inf)
    in
    let _calcul = expression' env' memory expr3 in
    boucle_for id (Mint.add borne_inf Mint.one) borne_sup expr3 env' memory)
  else VUnit (* Cas où nous ne sommes plus dans la boucle, on renvoie un VUnit *)

(** Assign: On commence par récupérer l'évaluation de expr1, qui correspond à la valeur
    à laquelle est affecté expr2 *)
and assign_value expr1 expr2 environment memory =
  let vall = value_as_location (expression' environment memory expr1) in
  (* On regarde ensuite si *)
  match vall with
  | None -> error [ expr1.position; expr2.position ] "erreur assign"
  | Some v -> assign_calcul environment memory expr2 v

and assign_calcul environment memory expr2 v =
  let value = expression' environment memory expr2 in
  let mem = Memory.dereference memory v in
  Memory.write mem Mint.zero value

(** While: Tant que la condition (valeur de l'expression 1) est true,
    alors on calcule l'expression de expression 2

    Sinon, on renvoie un VUnit, qui va stopper le processus d'interprétation
    pour While. *)
and while_value expr1 expr2 environment memory pos =
  let cond = expression' environment memory expr1 in
  (* On récupère la valeur de la condition *)
  match value_as_bool cond with
  | true ->
    let _expr = expression' environment memory expr2 in
    expression pos environment memory (While (expr1, expr2))
  | false -> VUnit

(** IfThenELse: Comme pour While, on regarde si la condition est vrai,
    si c'est le cas, alors on calcule l'expression du "then",
    sinon celle du "else" *)
and if_then_else_value expr1 expr2 expr3 environment memory =
  let cond = expression' environment memory expr1 in
  (* On récupère la valeur de la condition *)
  match value_as_bool cond with
  | true ->
    expression' environment memory expr2
    (* Si c'est true, alors on évalue la première expression *)
  | false -> expression' environment memory expr3 (* sinon la deuxième *)

(** Record: on calcule l'expression, et on associe à chaque élément
    de la liste la valeur du label *)
and field_value expr label environment memory =
  match expression' environment memory expr with
  | VRecord record -> List.assoc label.value record
  | _ -> assert false (* Cas où il n'y a pas de record, donc c'est une erreur *)

(** Fonction annexe qui renverra une paire label * valeur de expression *)
and pair_labels_gvalue environment memory (lab, expr) =
  Position.value lab, expression' environment memory expr

(** Apply: On applique la  fonction f à x en fonction des différents cas possibles *)
and apply_expression f x environment memory =
  let x_val = expression' environment memory x in
  match expression' environment memory f with
  | VPrimitive (_, f) ->
    (* Fonction "primitive" *)
    f memory x_val
  | VClosure (env_fn, pattern', expr) ->
    let valeur_pattern = pattern'.value in
    let pat = pattern env_fn valeur_pattern x_val in
    (* Pattern va nous calculer un nouvelle environnement *)
    (match pat with
     | Some env' -> expression' env' memory expr
     | None -> failwith "erreur")
  | _ -> assert false (* By typing *)

(** Littéraux *)
and literal_expression = function
  | LInt n -> VInt n
  | LChar c -> VChar c
  | LString s -> VString s

(* --------------- PATTERN MATCHING ----------------- *)

and pattern environment pat expression =
  match pat, expression with
  | PWildcard, _ -> wildcard_pattern environment
  | PLiteral lit, _ -> literal_pattern lit environment expression
  | PVariable var, _ -> variable_pattern var environment expression
  | PTypeAnnotation (pattern', _), _ ->
    typeannot_pattern pattern'.value environment expression
  | PTaggedValue (cons, _, new_pattern), VTagged (cons2, new_pattern2) ->
    tagged_pattern cons cons2 new_pattern new_pattern2 environment
  | PRecord (r, _), VRecord r2 -> record_pattern environment r r2
  | PTuple tab, VTuple tab2 -> tuple_pattern environment tab tab2
  | POr pl, _ -> or_pattern environment pl expression
  | PAnd pl, _ -> and_pattern environment pl expression
  | _ -> None

(** PWildcard: On renvoie l'environnement sans modification *)
and wildcard_pattern environment = Some environment

(** PVariable: On bind la valeur de la variable - l'expression dans l'environnement *)
and variable_pattern var env expression = Some (Environment.bind env var.value expression)

(** PTypeAnnotation: On calcule le pattern à son tour *)
and typeannot_pattern pattern' env expression = pattern env pattern' expression

(** PLiteral:  On regarde si la valeur du pattern correspond à celle de l'expression *)
and literal_pattern pl environment expression =
  let valeur_literal = pl.value in
  let verif_literal l1 l2 = if l1 = l2 then Some environment else None in
  match valeur_literal, expression with
  | LInt int1, VInt int2 -> verif_literal int1 int2
  | LChar char1, VChar char2 -> verif_literal char1 char2
  | LString str1, VString str2 -> verif_literal str1 str2
  | _ -> None

(** PTagged: On regarde si les deux constructeurs sont égaux, si c'est le cas
    on calcule comme si c'était un PTuple, les deux listes de patterns que
    nous avons *)
and tagged_pattern cons1 cons2 pattern1 pattern2 environment =
  if cons1.value = cons2 then tuple_pattern environment pattern1 pattern2 else None

(** PRecord: On compare les deux records, on essaye de trouver un match pour
    chaque field (du premier record) dans le deuxième record, si c'est le cas
    on met à jour l'env, sinon on regarde le prochain élément. *)
and record_pattern environment r r' =
  match r with
  | [] -> Some environment
  | field :: reste ->
    let labbel_pattern = record_label_pattern environment field r' in
    (match labbel_pattern with
     | Some env' -> record_pattern env' reste r'
     | None -> None)

(* Auxillaire *)
and record_label_pattern environment field r' =
  match r' with
  | [] -> None
  | (label, pat) :: reste ->
    if value (fst field) = label
    then (
      match pattern environment (value (snd field)) pat with
      | Some env' -> Some env'
      | None -> record_label_pattern environment field reste)
    else record_label_pattern environment field reste

(** PTuple: On a deux tuples, qui pour chaque pair d'éléments effectue
    un pattern matching, et renvoie le nouvel environnement *)
and tuple_pattern environment tab tab2 =
  if List.length tab = List.length tab2
  then (
    match tab, tab2 with
    | [], [] -> Some environment
    | pat :: tab', expr :: tab2' ->
      let valeur_pattern = pat.value in
      (match pattern environment valeur_pattern expr with
       | Some env' -> tuple_pattern env' tab' tab2'
       | None -> None)
    | _ -> None)
  else None

(** Por: Renvoie le nouvelle environnement si au moins un des
    patterns match avec l'expression *)
and or_pattern env pl expression =
  match pl with
  | [] -> None
  | p :: pl' ->
    let valeur_pattern = p.value in
    (match pattern env valeur_pattern expression with
     | Some env' -> Some env'
     | None -> or_pattern env pl' expression)

(** PAnd: De même, mais chaque élément de la liste des patterns doivent
    matchs avec l'expression *)
and and_pattern env pl expression =
  match pl with
  | [] -> Some env
  | p :: pl' ->
    let valeur_pattern = p.value in
    (match pattern env valeur_pattern expression with
     | None -> None
     | Some env' -> and_pattern env' pl' expression)

(** This function returns the difference between two runtimes. *)
and extract_observable runtime runtime' =
  let rec substract new_environment env env' =
    if env == env'
    then new_environment
    else (
      match Environment.last env' with
      | None -> assert false (* Absurd. *)
      | Some (x, v, env') ->
        let new_environment = Environment.bind new_environment x v in
        substract new_environment env env')
  in
  { new_environment = substract Environment.empty runtime.environment runtime'.environment
  ; new_memory = runtime'.memory
  }
;;

(** This function displays a difference between two runtimes. *)
let print_observable (_ : runtime) observation =
  Environment.print observation.new_memory observation.new_environment
;;
