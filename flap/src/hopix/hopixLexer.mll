{ (* -*- tuareg -*- *)
  open Lexing
  open Error
  open Position
  open HopixParser

  let next_line_and f lexbuf  =
    Lexing.new_line lexbuf;
    f lexbuf
  ;;

  let error lexbuf =
    let msg = "during lexing"
    in
    error msg (lex_join lexbuf.lex_start_p lexbuf.lex_curr_p)
  ;;

  let err_msg = "unexpected character."

  (* Fonction qui convertie une chaîne de caractère ascii en vrai caractère.
   * Notamment les escapes : "\n" ou "\000"
   * En plus de préserver les caractères "normaux" *)
  let recup_char data lexbuf =
    let length = String.length data in
    if length == 1
    then String.get data 0
    else (
      match data with
      | "\\n" -> '\n'
      | "\\b" -> '\b'
      | "\\r" -> '\r'
      | "\\t" -> '\t'
      | "\\\'" -> '\''
      | "\\\\" -> '\\'
      | _ ->
        (try
           let caractere = String.sub data 1 (length - 1) in
           let ascii_code = int_of_string caractere in
           Char.chr ascii_code
         with
         | _ -> error lexbuf err_msg))
  ;;
}


let newline = ('\010' | '\013' | "\013\010")
let blank   = [' ' '\009' '\012']

let hex_dig = ['0'-'9' 'a'-'f' 'A'-'F']

let digit = ['0'-'9']
let hexa = "0x" hex_dig+
let bina = "0b" ['0'-'1']+
let octa = "0o" ['0'-'7']+


(* Définition d'un atom
 * aka un string qui représente un char, par exemple "\065" = 'A' *)
let ascii_table = '\\' ['0'-'2'] ['0'-'9'] ['0'-'9'] (* TODO: on déborde de 255 à 299 :( *)
let ascii_hex = "\\0x" hex_dig hex_dig
let printable = ['\032'-'\038' '\040'-'\127']
let escapes = "\\n"
            | "\\b"
            | "\\r"
            | "\\t"
            | "\\'"
            | "\\\\"
let atom = ascii_table
         | ascii_hex
         | printable
         | escapes
         | '"'

(* On ne peut pas différencier au niveau du lexer var_id label_id et type_con,
 * il faudra le faire à l'analyseur syntaxique.
 * On va donc faire un 'ident' pour "identificateur" *)

(* Identificateurs var_id label_id type_con *)
let ident = ['a'-'z']['A'-'Z' 'a'-'z' '0'-'9' '_']*
(* Identificateur de constructeurs de données *)
let constr_id = ['A'-'Z']['A'-'Z' 'a'-'z' '0'-'9' '_']*
(* Identificateur de variables de type *)
let type_variable = '`' ident
(* Littéraux entiers *)
let int ='-'? digit+
            | hexa
            | bina
            | octa

(* Littéraux caractères *)
let char = (digit | ['A'-'Z'] | ['a'-'z'])

(* Quand le code ascii est trop grand
 * TODO: Ne se déclenche pas pour, par exemple, 270 :( *)
let ascii_trop_grand = '\\' ['3'-'9']['0'-'9'](['0'-'9'])+

(* Caractères d'un string *)
let str_char = ascii_table
             | ascii_hex
             | printable
             | escapes
             | '\''
             | "\\\""

(* Commentaires *)
let open_comment = "{*"
let close_comment = "*}"

rule token = parse
  (** Layout *)
  | newline               { next_line_and token lexbuf }
  | blank+                { token lexbuf               }
  | eof                   { EOF                        }
  | open_comment          { commentary 1 lexbuf        }
  | "##"                  { commentary_line lexbuf     }

  (** Keywords *)
  | "let"                 { LET    }
  | "type"                { TYPE   }
  | "extern"              { EXTERN }
  | "fun"                 { FUN    }
  | "match"               { MATCH  }
  | "if"                  { IF     }
  | "then"                { THEN   }
  | "else"                { ELSE   }
  | "ref"                 { REF    }
  | "while"               { WHILE  }
  | "do"                  { DO     }
  | "until"               { UNTIL  }
  | "from"                { FROM   }
  | "to"                  { TO     }
  | "and"                 { AND_KW }
  | "for"                 { FOR    }


  (** Ponctuation *)
  | '='                   { EQUAL     }
  | '('                   { LPAREN    }
  | ')'                   { RPAREN    }
  | '['                   { LBRACK    }
  | ']'                   { RBRACK    }
  | '{'                   { LBRACE    }
  | '}'                   { RBRACE    }
  | '_'                   { WILDCARD  }
  | ':'                   { COLON     }
  | ';'                   { SEMICOLON }
  | "->"                  { ARROW     }
  | '<'                   { INFERIOR  }
  | '>'                   { SUPERIOR  }
  | '|'                   { PIPE      }
  | '&'                   { AND       }
  | '*'                   { STAR      }
  | ','                   { COMMA     }
  | '.'                   { DOT       }
  | '\\'                  { BACKSLASH }
  | ":="                  { ASSIGN    }
  | '!'                   { EXCLA     }

  (* Opérateurs binaires *)
  | "+"                   { PLUS         }
  | "-"                   { MINUS        }
  | "/"                   { SLASH        }
  | "&&"                  { D_AND        }
  | "||"                  { D_OR         }
  | "=?"                  { EQUAL_OP     }
  | "<=?"                 { INF_EQUAL_OP }
  | ">=?"                 { SUP_EQUAL_OP }
  | "<?"                  { INF_OP       }
  | ">?"                  { SUP_OP       }

  (** Identificateurs *)
  | ident as s            { ID s  }
  | type_variable as s    { TID s }
  | constr_id as s        { CID s }

  (* Integers *)
  | int as i              { INT (Mint.of_string i) }

  (* Strings *)
  | '"'                   { read_string (Buffer.create 16) lexbuf }

  (* Characters *)
  | "'" (char as c) "'"   { CHAR c                     }
  | "'" (atom as a) "'"   { CHAR (recup_char a lexbuf) }

  (** Lexing errors *)
  (* Erreur qui advient quand un code ASCII est trop grand *)
  | "'" ascii_trop_grand "'" { error lexbuf ""      }
  | _                        { error lexbuf err_msg }

and commentary nest = parse
  (* Support nesting *)
  | close_comment { if nest = 1 then token lexbuf
                    else commentary (nest - 1) lexbuf      }
  | open_comment  { commentary (nest + 1) lexbuf           }

  (** Error *)
  | eof           { error lexbuf "unclosed commentary."    }

  (** Commentary content *)
  | newline       { next_line_and (commentary nest) lexbuf }
  | _             { commentary nest lexbuf                 }

and commentary_line = parse
  | newline { next_line_and token lexbuf }
  | eof     { EOF                        }
  | _       { commentary_line lexbuf     }

and read_string buffer = parse
  (** End of string *)
  | '"'           { STRING (Buffer.contents buffer)     }

  (** Escape  *)
  | "\\\""        { Buffer.add_char buffer '\"'
                  ; read_string buffer lexbuf           }

  (** String characters *)
  | str_char as s { let c = recup_char s lexbuf
                    in Buffer.add_char buffer c
                    ; read_string buffer lexbuf         }

  (** Error *)
  | eof           { error lexbuf "Unterminated string." }
